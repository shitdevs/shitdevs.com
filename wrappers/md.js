import React from 'react'
import moment from 'moment'
import Bio from 'components/Bio'
import DocumentTitle from 'react-document-title'
import { rhythm } from 'utils/typography'
import { config } from 'config'
import ReadNext from '../components/ReadNext'
import Footer from '../components/Footer'

import '../css/zenburn.css'

class MarkdownWrapper extends React.Component {
  render () {
    const { route } = this.props
    const post = route.page.data

    return (
      <DocumentTitle title={`${post.title} | ${config.blogTitle}`}>
        <div className="markdown">
          <h1 style={{ marginTop: 0 }}>{post.title}</h1>
          <div dangerouslySetInnerHTML={{ __html: post.body }} />
          <em
            style={{
              display: 'block',
              marginBottom: rhythm(2),
            }}
          >
            Posted {moment(post.date).format('MMMM D, YYYY')}
          </em>
          <hr
            style={{
              marginBottom: rhythm(2),
            }}
          />
          <ReadNext post={post} pages={route.pages} />
          <Bio author={post.author} />
          <Footer data={route.page} />
        </div>
      </DocumentTitle>
    )
  }
}

MarkdownWrapper.propTypes = {
  route: React.PropTypes.object,
}

export default MarkdownWrapper
