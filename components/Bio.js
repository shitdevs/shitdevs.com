import React, { PropTypes } from 'react'
import { config } from 'config'
import { rhythm } from 'utils/typography'
import find from 'lodash/fp/find'

class Bio extends React.Component {
  static propTypes = {
    author: PropTypes.string.isRequired,
  }
  static defaultProps = {
    author: 'Anonymous',
  }
  render () {
    const { author } = this.props
    const authorObj = find(obj => obj.name === author)(config.authors)
    return (
      <p
        style={{
          marginBottom: rhythm(2.5),
        }}
      >
        {authorObj.avatar
          ? <img
            src={authorObj.avatar}
            alt="Author"
            style={{
              float: 'left',
              marginRight: rhythm(1 / 4),
              marginBottom: 0,
              width: rhythm(2),
              height: rhythm(2),
              borderRadius: rhythm(0.25),
            }}
          />
          : null}
        {authorObj.bio
          ? <span>
            Written by <strong>{authorObj.name}</strong>
            who lives and works in San Francisco building useful things.
          </span>
          : <span>Written by <strong>{authorObj.name}</strong></span>
        }
        {authorObj.twitterHandle
          ? <span> | <a href={`https://twitter.com/${authorObj.twitterHandle}`}>You should follow them on Twitter</a></span>
          : null
        }
      </p>
    )
  }
}

export default Bio
