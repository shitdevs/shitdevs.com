import React, { PropTypes } from 'react'
import { config } from 'config'

const Footer = ({ data }) =>
  <footer>
    <a
      target="_blank"
      rel="noopener noreferrer"
      href={config.repoUrl}
    >Site Source</a>
    &nbsp;|&nbsp;
    <a
      href={`${config.repoUrl}/blob/master/pages/${data.file.base}`}
      target="_blank"
      rel="noopener noreferrer"
    >Page Source</a>
    &nbsp;|&nbsp;
    See an issue? <a
      href={`${config.issuesUrl}/new`}
      target="_blank"
      rel="noopener noreferrer"
    >Report it</a> or <a
      href={`${config.repoUrl}/edit/master/pages/${data.file.base}`}
      target="_blank"
      rel="noopener noreferrer"
    >fix it!</a>
    <br />
    Wanna write for us? <a
      href={`${config.repoUrl}/forks/new`}
      target="_blank"
      rel="noopener noreferrer"
    >Fork us</a> and create a merge request!
  </footer>

Footer.propTypes = {
  data: PropTypes.object.isRequired,
}

export default Footer
